// Array
let name = ['vish', 'gawhane', 'yash', 'Twi']
console.log(name.length)
name.forEach(function (item, index, array) {
    console.log(item, index)
})


//Function
// 1
function square(number) {
    return number * number;
}

let result = square(20)
console.log(result)

const addition = function (number) {
    return number * number
}
var x = addition(4)
console.log(x)
// -------------------------------------------------


// Arrow Function
let add = (x, y) => x + y;

console.log(add(10, 20))
// -------------------------------------------------


// String
const sentence = 'Hello Im Boy ';
console.log(sentence.charAt(4));
// -------------------------------------------------
console.log(sentence.charCodeAt(4));
// -------------------------------------------------

const icons = '☃★♲';
console.log(icons.codePointAt(1));
// -------------------------------------------------
const str1 = 'Vishal';
const str2 = 'Gawhane';

console.log(str1.concat(" " + str2));
// -------------------------------------------------


const strg1 = 'Dont loss hops!';

console.log(strg1.endsWith('hops', 14));
// -------------------------------------------------
console.log(String.fromCharCode(189, 43, 190, 61));
// -------------------------------------------------
const inc = 'The sun is hot.';
const word = 'is';
console.log(inc.includes(word));
// -------------------------------------------------


// Array Function

const a1 = ['a', 'b', 'c', 'd', 'e'];
console.log(a1.copyWithin(0, 3, 4));
// -------------------------------------------------

const a2 = ['a', 'b', 'c'];
const iterator1 = a2.entries();
console.log(iterator1.next().value)
// -------------------------------------------------

const a3 = [1, 2, 3, 4];
console.log(a3.fill(0, 2, 4));
// -------------------------------------------------

const a4 = ['a', 'b', 'c'];
a4.forEach(element => console.log(element))
// -------------------------------------------------

const a5 = ['a', 'b', 'c'];
const iterator = a5.keys();
for (const key of iterator) {
    console.log(key);
}
// -------------------------------------------------

const a6 = [1, 4, 9, 16];
const map1 = a6.map(x => x * 2);
console.log(map1);
// -------------------------------------------------

const a7 = [1, 2, 'a', '1a'];
console.log(a7.toString());
// -------------------------------------------------


/////////////////  Map and Set  ////////////////

let map = new Map();
map.set('1', 'str1');
map.set(1, 'num1');
map.set(true, 'bool1');
console.log(map.get(1));
console.log(map.get('1'));


/////////////////  Iteration protocols  ////////////////
const s1 = 'hi';
console.log(typeof s1[Symbol.iterator]);


/////////////////  JSON Method ////////////////
var J1 = '{"Name":"Vishal","Email": "Gawhane@gmail.com", "Mob": "12345"}';
var data = JSON.parse(J1);
console.log(data.Name)


var J2 = {Name:"Krishna", Email: "XYZ", CN : 12345};
var data2 = JSON.stringify(J2);
console.log(data2);


/////////////////  Object Method ////////////////
const person = {
    firstName: "Vishal",
    lastName: "Gawhane",
    age: 24
};
const p =  person
console.log(p.firstName)


/////////////////  New Operatior in js ////////////////
function fun1(a, b) {
    this.a = a;
    this.b = b;
}
var f = new fun1('vi',24)
console.log(f)

/////////////////  Optional chaining ////////////////
let user = {};
console.log(user.address ? user.address.street : undefined);





