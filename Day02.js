////////////////////////// OBJECT  /////////////////////\
const mark = {
    m1: 20,
    m2: 19,
    m3: 24
};
const m =  mark
console.log(m.m1)

/////////////////  New operatior in js ////////////////
function NO(name, age, mobileNO) {
    this.name = name;
    this.age = age;
    this.mobileNO = mobileNO;
}
var no = new NO('vi',24,9522)
console.log(no)

//////////////////  Object methods and this //////////

const person = {
    firstName: "Vishal",
    lastName: "gawhane",
    fullName: function() {
        return this.firstName + " " + this.lastName;
    }
};
var p1 = person
console.log(p1.fullName())

/////////////////// Variable scope /////////////////

// * Global Scope
var fruit = 'apple'
console.log(fruit);
function getFruit(){
    console.log(fruit);
}
console.log(getFruit())

// * Local Scope
function foo1(){
   var fruit = "apple"
    function foo2() {
        console.log(fruit)
    }
    foo2()

}
foo1()

// * Functional Scope
function foo3(){
    var fruit ='apple';
    console.log('inside function: ',fruit);
}

foo3();

//////////////////////// Closures


function init() {
    var name = 'Vishal';
    function displayName() {
        console.log(name);
    }
    displayName();
}
init();

////////////////////////// Callback Function

function F1(name) {
    console.log('Hello ' + name);
}
function finalFUnction(callback) {
    console.log("vishal")
}
finalFUnction(F1());


/////////////////////////// Inheritance
function Parent() {
    this.name = 'vishal';
    this.surname = 'gawhane';
}
function Child() {
    Parent.call(this);

    this.color = 'black';
}

console.log(Child())

/////////////  Inheritace Prototype

function Person(name, job, yearOfBirth){
    this.name= name;
    this.job= job;
    this.yearOfBirth= yearOfBirth;
}
// this will show Person's prototype property.
console.log(Person.prototype.name)


